<?php

namespace clases;

Class Imagen
{
    public string $nombre;
    private bool $border;
    private int $width;
    private int $height;

    const RUTA = "./images/";
    public function __construct(string $nombre,  bool $width=null, bool $height=null)    
    {
        $this->nombre = $nombre;
        $this->border = 0;
        $this->width = $width;
        $this->height = $height;
    }

     // Método para obtener la ruta completa de la imagen
     public function obtenerRutaCompleta() {
        return self::RUTA . $this->nombre;
    }

    // Método para devolver el objeto Imagen como cadena HTML
    public function __toString() {
        return "<img src='" . $this->obtenerRutaCompleta() . "' border='0' width='100' height='100'>";
    }
}