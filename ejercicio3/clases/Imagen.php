<?php

namespace clases;

class Imagen
{
    public string $nombre;
    private bool $border;
    private int $width;
    private int $height;

    const RUTA = "./images/";
    public function __construct()
    {
        $this->nombre = "";
        $this->border=0;
        $this->width = 0;
        $this->height = 0;
    }

     /**
     * Get the value of nombre
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of border
     */
    public function getBorder()
    {
        return $this->border;
    }

    /**
     * Set the value of border
     *
     * @return  self
     */
    public function setBorder($border)
    {
        $this->border = $border;

        return $this;
    }

    /**
     * Get the value of width
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set the value of width
     *
     * @return  self
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get the value of height
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set the value of height
     *
     * @return  self
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    // Método para obtener la ruta completa de la imagen
    public function obtenerRutaCompleta()
    {
        return self::RUTA . $this->nombre;
    }

    // Método para devolver el objeto Imagen como cadena HTML
    public function __toString()
    {
        return "<img src='" . $this->obtenerRutaCompleta() . "' border='" . $this->border . "' width='" . $this->width . "' height='" . $this->height . "'>";
    }

   
}
