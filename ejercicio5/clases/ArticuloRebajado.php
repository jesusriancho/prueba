<?php

namespace clases;

final class ArticuloRebajado extends Articulo{
    private float $rebaja;

    public function __construct(string $nombre, float $precio, float $rebaja = 0)    {
        parent::__construct($nombre, $precio);
        $this->rebaja = $rebaja;
    }

    private function calculaDescuento(): float{
        return $this->getPrecio() * $this->rebaja /100;
    }

    public function precioRebajado(): float{
        return $this->getPrecio() - $this->calculaDescuento();
    }

    public function __toString(): string{
        return parent::__toString() . "La Rebaja es: " . $this->rebaja . "%" . " y el descuento: " . $this->calculaDescuento() ."€";
    }

}
